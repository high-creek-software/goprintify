package order

import "gitlab.com/high-creek-software/goprintify/rest"

type Shipment struct {
	Carrier     string     `json:"carrier"`
	Number      string     `json:"number"`
	URL         string     `json:"url"`
	DeliveredAt rest.PTime `json:"delivered_at"`
}
