package order

import (
	"gitlab.com/high-creek-software/goprintify/rest"
)

type LineItem struct {
	ProductID          string           `json:"product_id"`
	Quantity           int              `json:"quantity"`
	VariantID          int              `json:"variant_id"`
	PrintProviderID    int              `json:"print_provider_id"`
	Cost               int              `json:"cost"`
	ShippingCost       int              `json:"shipping_cost"`
	Status             LineItemStatus   `json:"status"`
	Metadata           LineItemMetadata `json:"metadata"`
	SentToProductionAt rest.PTime       `json:"sent_to_production_at"`
	FulfilledAt        rest.PTime       `json:"fulfilled_at"`
}
