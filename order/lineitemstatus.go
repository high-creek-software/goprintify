package order

import (
	"encoding/json"
	"strings"
)

type LineItemStatus int

func (l *LineItemStatus) UnmarshalJSON(bytes []byte) error {
	str := strings.Trim(string(bytes), `"`)
	*l = LineItemStatusFromString(str)
	return nil
}

func (l LineItemStatus) MarshalJSON() ([]byte, error) {
	return json.Marshal(lineItemsStatuses[l])
}

var lineItemsStatuses = []string{"pending", "cancelled", "fulfilled", "unknown"}

const (
	LISPending LineItemStatus = iota
	LISCancelled
	LISFulfilled
	LISUnknown
)

func LineItemStatusFromString(s string) LineItemStatus {
	for idx, elem := range lineItemsStatuses {
		if elem == s {
			return LineItemStatus(idx)
		}
	}
	return LISUnknown
}
