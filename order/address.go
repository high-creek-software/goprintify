package order

type Address struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Region    string `json:"region"`
	Address1  string `json:"address1"`
	City      string `json:"city"`
	Zip       string `json:"zip"`
	Email     string `json:"email"`
	Phone     string `json:"phone"`
	Country   string `json:"country"`
	Company   string `json:"company"`
}
