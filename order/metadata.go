package order

import (
	"encoding/json"
	"gitlab.com/high-creek-software/goprintify/rest"
	"strings"
)

type LineItemMetadata struct {
	Title        string `json:"title"`
	Price        int    `json:"price"`
	VariantLabel string `json:"variant_label"`
	SKU          string `json:"sku"`
	Country      string `json:"country"`
}

type OrderMetadata struct {
	OrderType       OrderType  `json:"order_type"`
	ShopOrderID     int        `json:"shop_order_id"`
	ShopOrderLabel  string     `json:"shop_order_label"`
	ShopFulfilledAt rest.PTime `json:"shop_fulfilled_at"`
}

type OrderType int

func (o *OrderType) UnmarshalJSON(bytes []byte) error {
	str := strings.Trim(string(bytes), `"`)
	*o = OrderTypeFromString(str)
	return nil
}

func (o OrderType) MarshalJSON() ([]byte, error) {
	return json.Marshal(orderTypes[o])
}

var orderTypes = []string{"exernal", "manual", "sample", "unknown"}

const (
	OTExternal OrderType = iota
	OTManual
	OTSample
	OTUnknown
)

func OrderTypeFromString(s string) OrderType {
	for idx, elem := range orderTypes {
		if elem == s {
			return OrderType(idx)
		}
	}
	return OTUnknown
}
