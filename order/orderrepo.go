package order

import (
	"encoding/json"
	"fmt"
	"gitlab.com/high-creek-software/goprintify/rest"
	"log"
	"net/http"
)

const (
	listOrders = "shops/%d/orders.json"
)

type OrderRepo interface {
	ListOrders(shopID int) (PagedOrders, error)
}

type OrderRepoImpl struct {
	endpoint *rest.Endpoint
}

func (o OrderRepoImpl) ListOrders(shopID int) (PagedOrders, error) {
	req, err := o.endpoint.NewGetRequest(fmt.Sprintf(listOrders, shopID))
	if err != nil {
		return PagedOrders{}, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return PagedOrders{}, err
	}
	defer resp.Body.Close()

	log.Println(resp.StatusCode)

	var pagedOrders PagedOrders
	err = json.NewDecoder(resp.Body).Decode(&pagedOrders)

	return pagedOrders, err
}

func NewOrderRepo(endpoint *rest.Endpoint) OrderRepo {
	return &OrderRepoImpl{endpoint: endpoint}
}

type PagedOrders struct {
	rest.Paging
	Data []Order `json:"data"`
}
