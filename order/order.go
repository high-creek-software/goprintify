package order

import (
	"encoding/json"
	"gitlab.com/high-creek-software/goprintify/rest"
	"strings"
)

type Order struct {
	ID                 string        `json:"id"`
	AddressTo          Address       `json:"address_to"`
	LineItems          []LineItem    `json:"line_items"`
	Metadata           OrderMetadata `json:"metadata"`
	TotalPrice         int           `json:"total_price"`
	TotalShipping      int           `json:"total_shipping"`
	TotalTax           int           `json:"total_tax"`
	Status             OrderStatus   `json:"status"`
	ShippingMethod     int           `json:"shipping_method"`
	Shipments          []Shipment    `json:"shipments"`
	CreatedAt          rest.PTime    `json:"created_at"`
	SentToProductionAt rest.PTime    `json:"sent_to_production_at"`
	FulfilledAt        rest.PTime    `json:"fulfilled_at"`
}

type OrderStatus int

func (o *OrderStatus) UnmarshalJSON(bytes []byte) error {
	str := strings.Trim(string(bytes), `"`)
	*o = OrderStatusFromString(str)
	return nil
}

func (o OrderStatus) MarshalJSON() ([]byte, error) {
	return json.Marshal(orderStatuses[o])
}

var orderStatuses = []string{"pending", "on-hold", "checking-quality", "quality-declined", "quality-approved",
	"ready-for-production", "sending-to-production", "in-production", "cancelled", "fulfilled",
	"partially-fulfilled", "payment-not-received", "callback-received", "has-issues", "unknown"}

const (
	OSPending OrderStatus = iota
	OSOnHold
	OSCheckingQuality
	OSQualityDeclined
	OSQualityApproved
	OSReadyForProduction
	OSSendingToProduction
	OSInProduction
	OSCancelled
	OSFulfilled
	OSPartiallyFulfilled
	OSPaymentNotReceived
	OSCallbackReceived
	OSHasIssues
	OSUnknown
)

func OrderStatusFromString(s string) OrderStatus {
	for idx, elem := range orderStatuses {
		if elem == s {
			return OrderStatus(idx)
		}
	}
	return OSUnknown
}
