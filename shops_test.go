package goprintify_test

import (
	"gitlab.com/high-creek-software/goprintify"
	"log"
	"os"
	"testing"
)

func TestShopsList(t *testing.T) {
	key := os.Getenv("AUTH_KEY")
	if key == "" {
		t.Fatalf("expected auth key")
	}

	client := goprintify.NewClient(key)

	shops, err := client.ListShops()
	if err != nil {
		t.Fatal("error loading shops", err)
	}

	log.Println(shops)
}
