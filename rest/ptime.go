package rest

import (
	"encoding/json"
	"strings"
	"time"
)

const (
	pTimeFormat = "2006-01-02 15:04:05+00:00"
)

type PTime time.Time

func (p *PTime) UnmarshalJSON(bytes []byte) error {
	str := strings.Trim(string(bytes), `"`)

	t, err := time.Parse(pTimeFormat, str)
	if err != nil {
		return err
	}

	*p = PTime(t)

	return nil
}

func (p PTime) MarshalJSON() ([]byte, error) {
	res := time.Time(p).Format(pTimeFormat)
	return json.Marshal(res)
}
