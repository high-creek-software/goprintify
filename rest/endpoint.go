package rest

import (
	"fmt"
	"net/http"
)

const (
	headerAccept        = "Accept"
	headerContentType   = "Content-Type"
	applicationJson     = "application/json"
	headerAuthorization = "Authorization"
	formatBearer        = "Bearer %s"
)

type Endpoint struct {
	apiKey  string
	baseURL string

	token string
}

func NewEndpoint(apiKey, baseURL string) *Endpoint {
	return &Endpoint{apiKey: apiKey, baseURL: baseURL, token: fmt.Sprintf(formatBearer, apiKey)}
}

func (e *Endpoint) NewGetRequest(uri string) (*http.Request, error) {
	url := e.baseURL + uri

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add(headerAuthorization, e.token)
	req.Header.Add(headerAccept, applicationJson)

	return req, nil
}
