package goprintify_test

import (
	"fmt"
	"gitlab.com/high-creek-software/goprintify"
	"os"
	"strconv"
	"testing"
)

func TestListOrders(t *testing.T) {
	key := os.Getenv("AUTH_KEY")
	if key == "" {
		t.Fatal("expected auth key")
	}

	shopIDSTr := os.Getenv("SHOP_ID")
	if shopIDSTr == "" {
		t.Fatal("expected shop id")
	}

	shopID, err := strconv.Atoi(shopIDSTr)
	if err != nil {
		t.Fatal("error parsing shop id", err)
	}

	client := goprintify.NewClient(key)

	orders, err := client.ListOrders(shopID)
	if err != nil {
		t.Fatal("error listing orders", err)
	}

	fmt.Println(orders)
}
