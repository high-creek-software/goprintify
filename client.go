package goprintify

import (
	"gitlab.com/high-creek-software/goprintify/order"
	"gitlab.com/high-creek-software/goprintify/product"
	"gitlab.com/high-creek-software/goprintify/rest"
	"gitlab.com/high-creek-software/goprintify/shops"
)

const (
	baseURL = "https://api.printify.com/v1/"
)

type Client struct {
	endpoint *rest.Endpoint
	shops.ShopRepo
	product.ProductRepo
	order.OrderRepo
}

type ClientConfig func(c *Client)

func NewClient(apiKey string, opts ...ClientConfig) *Client {

	endpoint := rest.NewEndpoint(apiKey, baseURL)
	client := &Client{endpoint: endpoint}
	client.ShopRepo = shops.NewShopRepo(endpoint)
	client.ProductRepo = product.NewProductRepo(endpoint)
	client.OrderRepo = order.NewOrderRepo(endpoint)

	for _, opt := range opts {
		opt(client)
	}

	return client
}
