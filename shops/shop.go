package shops

type Shop struct {
	ID           int    `json:"id"`
	Title        string `json:"title"`
	SalesChannel string `json:"sales_channel"`
}
