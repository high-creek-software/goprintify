package shops

import (
	"encoding/json"
	"gitlab.com/high-creek-software/goprintify/rest"
	"log"
	"net/http"
)

const (
	listShops = "shops.json"
)

type ShopRepo interface {
	ListShops() ([]Shop, error)
}

type ShopRepoImpl struct {
	endpoint *rest.Endpoint
}

func NewShopRepo(endpoint *rest.Endpoint) ShopRepo {
	return &ShopRepoImpl{endpoint: endpoint}
}

func (s *ShopRepoImpl) ListShops() ([]Shop, error) {

	req, err := s.endpoint.NewGetRequest(listShops)
	if err != nil {
		return nil, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	log.Println(resp.StatusCode)

	var shops []Shop
	err = json.NewDecoder(resp.Body).Decode(&shops)

	return shops, err
}
