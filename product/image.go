package product

type Image struct {
	Src        string `json:"src"`
	VariantIDs []int  `json:"variant_ids"`
	Position   string `json:"position"` // TODO: Create a position type
	IsDefault  bool   `json:"is_default"`
}
