package product

import (
	"encoding/json"
	"fmt"
	"gitlab.com/high-creek-software/goprintify/rest"
	"log"
	"net/http"
)

const (
	listProducts = "shops/%d/products.json"
	getProduct   = "shops/%d/products/%s.json"
)

type ProductRepo interface {
	ListProducts(shopID int) (PagedProducts, error)
	GetProduct(shopID int, productID string) (Product, error)
}

type ProductRepoImpl struct {
	endpoint *rest.Endpoint
}

type PagedProducts struct {
	rest.Paging
	Data []Product `json:"data"`
}

func NewProductRepo(e *rest.Endpoint) ProductRepo {
	return &ProductRepoImpl{endpoint: e}
}

func (pr *ProductRepoImpl) ListProducts(shopID int) (PagedProducts, error) {

	req, err := pr.endpoint.NewGetRequest(fmt.Sprintf(listProducts, shopID))
	if err != nil {
		return PagedProducts{}, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return PagedProducts{}, err
	}
	defer resp.Body.Close()

	log.Println(resp.StatusCode)

	var pagedProducts PagedProducts
	err = json.NewDecoder(resp.Body).Decode(&pagedProducts)

	return pagedProducts, err
}

func (pr *ProductRepoImpl) GetProduct(shopID int, productID string) (Product, error) {
	req, err := pr.endpoint.NewGetRequest(fmt.Sprintf(getProduct, shopID, productID))
	if err != nil {
		return Product{}, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return Product{}, err
	}
	defer resp.Body.Close()

	log.Println(resp.StatusCode)

	var product Product
	err = json.NewDecoder(resp.Body).Decode(&product)
	return product, err
}
