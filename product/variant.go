package product

type Variant struct {
	ID          int    `json:"id"`
	Price       int    `json:"price"`
	Cost        int    `json:"cost"`
	Title       string `json:"title"`
	Sku         string `json:"sku"`
	Grams       int    `json:"grams"`
	IsEnabled   bool   `json:"is_enabled"`
	IsDefault   bool   `json:"is_default"`
	IsAvailable bool   `json:"is_available"`
	Options     []int  `json:"options"`
}
