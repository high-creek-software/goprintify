package product

type PrintArea struct {
	VariantIDs   []int         `json:"variant_ids"`
	Placeholders []Placeholder `json:"placeholders"`
}

type Placeholder struct {
	Position string  `json:"position"`
	Images   []Image `json:"images"`
}
