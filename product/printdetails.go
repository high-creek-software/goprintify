package product

import (
	"encoding/json"
	"strings"
)

type PrintDetails struct {
	PrintOnSide PrintOnSide `json:"print_on_side"`
}

type PrintOnSide int

const (
	PrintOnSideUnknown PrintOnSide = iota
	PrintOnSideRegular
	PrintOnSideMirror
	PrintOnSideOff
)

var printOnSideOptions = []string{"Unknown", "Regular", "Mirror", "Off"}

func (p PrintOnSide) String() string {
	return printOnSideOptions[p]
}

func PriontOnSideFromString(p string) PrintOnSide {
	for idx, elem := range printOnSideOptions {
		if elem == p {
			return PrintOnSide(idx)
		}
	}
	return PrintOnSideUnknown
}

func (p *PrintOnSide) UnmarshalJSON(b []byte) error {
	str := strings.Trim(string(b), `"`)
	*p = PriontOnSideFromString(str)
	return nil
}

func (p PrintOnSide) MarshalJSON() ([]byte, error) {
	return json.Marshal(p.String())
}
