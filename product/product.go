package product

import (
	"gitlab.com/high-creek-software/goprintify/rest"
)

type Product struct {
	ID              string         `json:"id"`
	Title           string         `json:"title"`
	Description     string         `json:"description"`
	Tags            []string       `json:"tags"`
	Options         []Option       `json:"options"`
	Variants        []Variant      `json:"variants"`
	Images          []Image        `json:"images"`
	CreatedAt       rest.PTime     `json:"created_at"`
	UpdateAt        rest.PTime     `json:"update_at"`
	Visible         bool           `json:"visible"`
	BlueprintID     int            `json:"blueprint_id"`
	PrintProviderID int            `json:"print_provider_id"`
	UserID          int            `json:"user_id"`
	ShopID          int            `json:"shop_id"`
	PrintAreas      []PrintArea    `json:"print_areas"`
	PrintDetails    []PrintDetails `json:"print_details"`
	External        []External     `json:"external"`
	IsLocked        bool           `json:"is_locked"`
}
