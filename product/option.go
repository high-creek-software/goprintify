package product

type Option struct {
	Name   string        `json:"name"`
	Type   string        `json:"type"`
	Values []OptionValue `json:"values"`
}

type OptionValue struct {
	ID     int      `json:"id"`
	Title  string   `json:"title"`
	Colors []string `json:"colors"`
}
