package goprintify_test

import (
	"fmt"
	"gitlab.com/high-creek-software/goprintify"
	"os"
	"strconv"
	"testing"
)

func TestListProducts(t *testing.T) {
	key := os.Getenv("AUTH_KEY")
	if key == "" {
		t.Fatal("expected auth key")
	}

	shopIDSTr := os.Getenv("SHOP_ID")
	if shopIDSTr == "" {
		t.Fatal("expected shop id")
	}

	shopID, err := strconv.Atoi(shopIDSTr)
	if err != nil {
		t.Fatal("error parsing shop id", err)
	}

	client := goprintify.NewClient(key)

	products, err := client.ListProducts(shopID)
	if err != nil {
		t.Fatal("error listing products", err)
	}

	fmt.Println(products)
}

func TestGetProduct(t *testing.T) {
	key := os.Getenv("AUTH_KEY")
	if key == "" {
		t.Fatal("expected auth key")
	}

	shopIDSTr := os.Getenv("SHOP_ID")
	if shopIDSTr == "" {
		t.Fatal("expected shop id")
	}

	shopID, err := strconv.Atoi(shopIDSTr)
	if err != nil {
		t.Fatal("error parsing shop id", err)
	}

	client := goprintify.NewClient(key)
	productID := "637b81bbe7c0702388055f92"

	product, err := client.GetProduct(shopID, productID)
	if err != nil {
		t.Fatal("error getting products", err)
	}

	fmt.Println(product)
}
